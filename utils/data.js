class TyperData {
    constructor () {
        this.games = [];
    }
    addGame(_textData, _id){
        var textData = _textData;
        var id = _id;
        var game = {textData,id};
        this.games.push(game);
        console.log('data on game '+ textData);
        return game;
    }
    removeGame(hostId){
        var game = this.getGame(hostId);
        
        if(game){
            this.games = this.games.filter((game) => game.hostId !== hostId);
        }
        return game;
    }
    getGame(hostId){
        return this.games.filter((game) => game.id === hostId)[0]
    }
}

module.exports = {TyperData};