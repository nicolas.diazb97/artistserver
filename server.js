
const {TyperData} = require('./utils/data');
var games = new TyperData();
var port = process.env.PORT || 3000,
    io2 = require('socket.io')(port),
    gameSocket = null;
    console.log('server open: texts on game: '+games.games.length);
gameSocket = io2.on('connection', function(socket){
    console.log('socket connected: ' + socket.id);
io2.emit('message', "this is a test");
    socket.on('disconnect', function(){
        console.log('socket disconnected: ' + socket.id);
    });

    socket.on('test-event1', function(){
        console.log('got test-event1');
    });

    socket.on('deletetyperdata', function(){
        games.games = [];
    });

    socket.on('test-event2', function(data){
        console.log('got test-event2');
        console.log(data);

        io2.emit('test-event', {
            x: data.x,
            y: data.y,
            z: data.z
        });
    });

    socket.on('test-event4', function(data){
        console.log('got test-event2');
        console.log(data);
        var game = games.getGame(data.textId); 
        if(game)
        {
            console.log('text update '+game.textData+' - '+ data.currText);
            game.textData = data;
            console.log('text update '+ game.textData);
        }
        else
        {
            games.addGame(data,data.textId);
            console.log('text added');
        }
        
    for(var i = 0; i < games.games.length; i++)
    {
        io2.emit('test-event5', games.games[i].textData); 
    }
    });
    socket.on('new-audio', function(data){
        
        io2.emit('test-event7', data); 
        
    });
    socket.on('new-reset', function(data){
        
        io2.emit('resetavistamiento', data); 
        
    });
    socket.on('new-toggle', function(data){
        
        io2.emit('seton', data); 
        
    });
    socket.on('emit-pos-update', function(data){
        
        io2.emit('online-drag', data); 
        console.log('emitido ' + data.atid);
        
    });
    socket.on('new-toggleoff', function(data){
        
        io2.emit('setoff', data); 
        
    });

    socket.on('test-event3', function(data, callback){
        console.log('got test-event3');
        console.log(data);

        callback({
            test: 123456,
            test2: "test3"
        });
    });


});
